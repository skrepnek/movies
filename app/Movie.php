<?php

namespace Movies;

class Movie extends TheMovieDBAPI
{
    /**
     * ID of the movie
     *
     * @var int
     */
    public $id;

    /**
     * Title of the movie
     *
     * @var string
     */
    public $title;

    /**
     * Absolute or relative path to the movie's poster
     *
     * @var string
     */
    public $poster;

    /**
     * List of genres comma separated
     *
     * @var string
     */
    public $genres;

    /**
     * Overview of the movie
     *
     * @var string
     */
    public $overview;

    /**
     * Release date of the movie
     * // NOTE: This variable could be of type mixed and
     * // have date as DateTime object or string for TBA.
     * // Formating would be ideal on print
     *
     * @var string
     */
    public $releaseDate;

    /**
     * Score of the movie
     *
     * @var double
     */
    public $score;

    /**
     * Create a new Movie instance.
     *
     * @param int $id
     * @param string $title
     * @param string $poster
     * @param string $overview
     * @param string $releaseDate
     * @param string $releaseYear (Not used ATM)
     * @param double $score
     *
     * @return void
     */
    public function __construct($id, $title="", $poster="", $genres="", $overview="", $releaseDate="TBA", $score)
    {
        $this->id = $id;
        $this->title = $title;
        if(empty($poster))
        {
            $this->poster = "images/poster_not_available.png";
        }
        else
        {
            $this->poster = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/".$poster;
        }
        $this->genres = $genres;
        $this->overview = $overview;
        $this->releaseDate = $releaseDate;
        $this->score = $score;
    }

    /**
     * Retrieve all movies from a genre.
     *
     * @param int $genreId
     * @param int $page
     *
     * @return object
     */
    public static function allFromGenre(int $genreId, int $page): object
    {
        $object = "discover/movie";
        $parameters = 'sort_by=popularity.desc&include_adult=false&include_video=false&page='.$page.'&with_genres='.$genreId;
        return self::request($object, $parameters);
    }

    /**
     * Retrieve all movies from a genre.
     *
     * @param string $title
     * @param int $page
     *
     * @return object
     */
    public static function allFromTitle(string $title, int $page): object
    {
        $object = "search/movie";
        $parameters = 'page='.$page.'&query='.urlencode($title);
        return self::request($object, $parameters);
    }
}
