<?php

namespace Movies;

class Genre extends TheMovieDBAPI
{
    public $id;
    public $name;

    /**
     * Create a new genre instance.
     *
     * @param int $id
     * @param string $name
     *
     * @return void
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Retrieve all genres.
     *
     * @return array of Genre
     */
    public static function all()
    {
        $object = 'genre/movie/list';
        return self::request($object)->genres;
    }
}
