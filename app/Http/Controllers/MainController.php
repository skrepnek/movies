<?php

namespace Movies\Http\Controllers;


use GuzzleHttp\Psr7\Response;
use Movies\Genre;
use Movies\Movie;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Initialize movies session Cache
        if(!session()->exists('movieList'))
        {
            session()->put('movieList', []);
        }

        // Get genreList from Session. Otherwise do API request
        if(session()->has('genreList'))
        {
            $genreList = session()->get('genreList');
        }
        else
        {
            try
            {
                $genres = Genre::all();
            }
            catch (ClientException $exception) {
                return back()->withError($exception->getMessage())->withInput();
            }
            $genreList = array();
            foreach($genres as $k => $genre)
            {
                $genreList[$genre->id] = new Genre($genre->id, $genre->name);
            }
            session()->put('genreList', $genreList);
        }

        return view('home', [
            'genres' => $genreList
        ]);
    }

    /**
     * Search all movies from a type
     *
     * @param string $type
     * @param mixed $data
     * @param int $page
     *
     * @return string
     */
    public function searchMovies(string $type, string $data, string $page): string
    {
        $page = (int)$page;
        // First we request the Movies data
        if($type=='genre')
        {
            $data = (int)$data;
            $results = Movie::allFromGenre($data, $page);
        }
        else if($type=='title')
        {
            $data = (string)$data;
            $results = Movie::allFromTitle($data, $page);
        }

        // We add the pagination parameters
        $results->paginationFirst = max($results->page-3, 1);
        $results->paginationLast = min(max($results->page+3, 7), $results->total_pages);
        $results->firstItem = min(1+(20*($results->page-1)),$results->total_results);
        $results->lastItem = min(20*$results->page, $results->total_results);
        $results->paginationPrevious = "";
        $results->paginationNext = "";
        if($page == 1)
        {
            $results->paginationPrevious = "disabled";
        }
        if($page == $results->total_pages)
        {
            $results->paginationNext = "disabled";
        }

        // Now we iterate though the results for 2 porpuses:
        // - Generate the genre list (ids are not useful for the user)
        // - Cache on Session the movies displayed;
        $sessionMovies = array();
        foreach($results->results as $k => $movie)
        {
            // Generate genre list
            $genreList = session('genreList');
            $genres = "";
            foreach($movie->genre_ids as $genreId)
            {
                if (is_array($genreList) && array_key_exists($genreId, $genreList))
                {
                    $genres .= $genreList[$genreId]->name . ", ";
                }
            }
            $results->results[$k]->genres = rtrim($genres, ', ');

            $movie->release_date = date('jS F Y', strtotime($movie->release_date));

            $sessionMovies[$movie->id] = new Movie($movie->id, $movie->title, $movie->poster_path,
                $results->results[$k]->genres, $movie->overview, $movie->release_date, $movie->vote_average);
            session()->put('movieList', $sessionMovies);
        }

        // Wrap it up and display
        $page = view('movielist', [
            'movielist' => $results
        ]);

        return json_encode($page->render());
    }

    /**
     * Load the info of a movie
     *
     * @param int $id
     *
     * @return string
     */
    public function loadMovie(int $id): string
    {
        $movie = null;
        $movieList = session()->get('movieList');
        if(is_array($movieList) && array_key_exists($id, $movieList))
        {
            $movie = session()->get('movieList')[$id];
        }
        else
        {
            // We can throw here an exception (as this condition is outside
            // the scope of the project) or try to search through the API
            // and then, if not found, throw an exception (extending the
            // scope of the project)
        }
        // Wrap it up and display
        $page = view('movie', [
            'movie' => $movie
        ]);

        return $page->render();
    }
}
