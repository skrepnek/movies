<?php

namespace Movies;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

abstract class TheMovieDBAPI
{
    /**
     * Send the request to the API and return the JSON decoded
     * Requests split in two components: object and parameters
     *
     * @param string $object
     * @param string parameters
     *
     * @return object
     */
    protected static function request(string $object, string $parameters=""): object
    {
        if(!env('MOVIE_DB_API_KEY'))
        {
            throw new Exceptions\InvalidMovieDBAPIKeyException("The Movie DB API Key is invalid. Please verify .env file.", 1);
        }
        if($parameters != "")
        {
            $parameters .= "&";
        }
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.themoviedb.org/3/'.$object.'?'.$parameters.'api_key='.env('MOVIE_DB_API_KEY'));
        $body = $response->getBody();
        return json_decode($body);
    }
}
