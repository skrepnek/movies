$(document).ready(function() {
    $('#genres').change(function() {
        if($('#genres').val() > 0){
            searchMovies('genre', 1);
        }
        else{
            $('#results').html("");
            $('#results').toggleClass('invisible',true);
        }
    });

    $('#searchForm').submit(function(event) {
        searchMovies('title', 1);
        event.preventDefault();
    });
});

function setupResultEvents(type){
    $('.page-link').click(function() {
        searchMovies(type, $(this).data('page'));
    });

    $('.movie-row').click(function() {
        $('.modal-content').load('/movie/'+$(this).data('id'), function(){
            $('#bootstrap-modal').modal({
				show : true
			});
        });
    });
}

function searchMovies(type, page){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var url = '/movie/'+type+'/'
    var runAjax = false;
    if(type=='genre' && $('#genres').val() > 0){
        url += $('#genres').val()
        runAjax = true;
    }
    else if(type=='title' && $('#search').val()){
        url += sanitizeString($('#search').val())
        runAjax = true;
    }
    url += '/'+page

    if(runAjax){
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function(data) {
                console.log('success');
                $('#results').html(data);
                $('#results').toggleClass('invisible',false);
                setupResultEvents(type);
            },
            error: function(data) {
                console.log('error');
                $('#results').html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Apologies. The request could not be fulfilled. Please try again later.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                $('#results').toggleClass('invisible',false);
            }
        });
    }
}

function sanitizeString(str){
    str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"");
    return str.trim();
}
