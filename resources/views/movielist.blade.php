@extends('layouts.empty')

@section('content')

<div class="card-header">Results</div>
<div class="card-body">
    @if ($movielist->total_results > 0)
    <p class="text-right">Showing {{ $movielist->firstItem }} to {{ $movielist->lastItem }} of {{ $movielist->total_results }} results.</p>
    <nav aria-label="Search results pages">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" data-page="1">First</a>
            </li>
            <li class="page-item {{ $movielist->paginationPrevious }}">
                <a class="page-link" href="#" data-page="{{ $movielist->page-1 }}">Previous</a>
            </li>
            @for ($i = $movielist->paginationFirst; $i <= $movielist->paginationLast; $i++)
                @if ($i == $movielist->page)
                    <li class="page-item active"><a class="page-link" href="#" data-page="{{ $i }}">{{ $i }} <span class="sr-only">(current)</span></a></li>
                @else
                    <li class="page-item"><a class="page-link" href="#" data-page="{{ $i }}">{{ $i }}</a></li>
                @endif
            @endfor
            <li class="page-item {{ $movielist->paginationNext }}">
                <a class="page-link" href="#" data-page="{{ $movielist->page+1 }}">Next</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#" data-page="{{ $movielist->total_pages }}">Last</a>
            </li>
        </ul>
    </nav>
    <table class="table table-striped table-hover">

        <!-- Table Headings -->
        <thead>
            <th>Name</th>
            <th>Genres</th>
            <th>Popularity</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($movielist->results as $movie)
                <tr class='movie-row' data-id='{{ $movie->id }}' data-toggle="modal" data-target="#movieModal">
                    <!-- Movie Title -->
                    <td class="table-text">
                        <div>{{ $movie->title }}</div>
                    </td>
                    <td class="table-text">
                        <div>{{ $movie->genres }}</div>
                    </td>
                    <td class="table-text">
                        <div>{{ $movie->popularity }}</div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <nav aria-label="Search results pages">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" data-page="1">First</a>
            </li>
            <li class="page-item {{ $movielist->paginationPrevious }}">
                <a class="page-link" href="#" data-page="{{ $movielist->page-1 }}">Previous</a>
            </li>
            @for ($i = $movielist->paginationFirst; $i <= $movielist->paginationLast; $i++)
                @if ($i == $movielist->page)
                    <li class="page-item active"><a class="page-link" href="#" data-page="{{ $i }}">{{ $i }} <span class="sr-only">(current)</span></a></li>
                @else
                    <li class="page-item"><a class="page-link" href="#" data-page="{{ $i }}">{{ $i }}</a></li>
                @endif
            @endfor
            <li class="page-item {{ $movielist->paginationNext }}">
                <a class="page-link" href="#" data-page="{{ $movielist->page+1 }}">Next</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#" data-page="{{ $movielist->total_pages }}">Last</a>
            </li>
        </ul>
    </nav>
    @else
    <h3 class="text-center">No movies found</h3>
    @endif
</div>

@endsection
