@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <!-- Search Card -->
            <div class="card">
                <div class="card-header">Search Options</div>
                <div class="card-body">
                    <div class="row">
                        <!-- Search for titles -->
                        <div class="col-md-6">
                            <form id='searchForm'>
                                <label for="search">By Title</label>
                                <div class="input-group">
                                    <input type="text" name="search" id="search" class="form-control" placeholder="Start typing...">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- List of Genres -->
                        @if (count($genres) > 0)
                        <div class="col-md-6 border-left">
                            <label for="genres">Or by Genre</label>
                            <select name="genres" id="genres" class="custom-select d-block w-100">
                                <option value="0">Select a genre...</option>
                                @foreach ($genres as $genre)
                                    <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Results Card -->
            <div class="card invisible" id="results"></div>
        </div>
    </div>
</div>

<!-- Movie Modal -->
<div class="modal fade" id="movieModal" tabindex="-1" role="dialog" aria-labelledby="movieModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

@endsection
