@extends('layouts.empty')

@section('content')

@if (!empty($movie))
<div class="modal-header">
    <h2 class="modal-title" id="movieTitle">{{ $movie->title }}</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-5">
              <img class="img-fluid rounded img-thumbnail" src="{{ $movie->poster }}"></img>
          </div>
          <div class="col-md-7 ml-auto">
              <h3>Overview</h3>
              <p>{{ $movie->overview }}</p>
              <h3>Score</h3>
              <p>{{ $movie->score }}</p>
              @if (!empty($movie->genres))
              <h3>Genres</h3>
              <p>{{ $movie->genres }}</p>
              @endif
              <h3>Release Date</h3>
              <p>{{ $movie->releaseDate }}</p>
          </div>
        </div>
</div>
<div class="modal-footer" style="margin-top: 16px;">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
@endif
